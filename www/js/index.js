var menuIsOpen = false;
var pages = null;
var currentPage = "page_home";

loadPages = function() {
    pages = {
        page_home: document.getElementById("page-home"),
        page_wifi: document.getElementById("page-wifi"),
        page_bluetooth: document.getElementById("page-bluetooth"),
        page_screen: document.getElementById("page-screen"),
        page_battery: document.getElementById("page-battery"),
        page_gps: document.getElementById("page-gps")
    }
}

var events = {
    /*
    Eventos de una app en PhoneGap
    --------------------------------
    deviceready
    pause
    resume
    backbutton
    menubutton
    searchbutton
    startcallbutton
    endcallbutton
    volumedownbutton
    volumeupbutton
*/
    deviceReady: function() {
        // console.log("Aplicación iniciada");
    },
    // evento disparado cuando todo el HTML ha sido cargado
    contentLoaded: function() {
        loadPages();
        FastClick.attach(document.body);
    },
    backButton: function() {
        // navigator.app.exitApp();
    }
}

// listener evento de dispositivo listo
document.addEventListener('deviceready', events.deviceReady, false);
// listener de contenido DOM listo
document.addEventListener('DOMContentLoaded', events.contentLoaded, false);

// función click en menú
function menuButtonClick() {
    console.log("button clicked");
    if (menuIsOpen) {
        pages[currentPage].className = "page transition center";
        menuIsOpen = false;
    } else {
        pages[currentPage].className = "page transition right";
        menuIsOpen = true;
    }
}

// función cambiar página
function changePage(pageSelected) {
    console.log(pageSelected);
    pages[currentPage].className = "hidden";
    
    pages[pageSelected].className = "page right";
    setTimeout(function() {
        pages[pageSelected].className = "page visible transition center";
        currentPage = pageSelected;
        menuIsOpen = false;
    }, 0);
}